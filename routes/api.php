<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\TodoListController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::prefix('todolist')->group(function () {
    Route::get('delete/{id}', [TodoListController::class, 'getDelete']);
    Route::post('update/{id}', [TodoListController::class, 'postUpdate']);
    Route::post('create', [TodoListController::class, 'postCreate']);
    Route::get('list', [TodoListController::class, 'getList']);
});

Route::prefix('auth')->group(function () {
    Route::get('google/redirect', [AuthController::class, 'redirectGoogle']);
    Route::get('google/callback', [AuthController::class, 'callbackGoogle']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
