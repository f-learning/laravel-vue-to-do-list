<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Validator;

class TodoListController extends Controller
{
    // menampilkan data (Read)
    public function getList()
    {
        $result = DB::table("todolist")->orderby('id', "desc")->get();
        return response()->json($result);
    }

    // menambahkan data (Create)
    public function postCreate()
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
        ]);

        //response validator failed
        if ($validator->fails()) {
            $response = [
                'status' => false,
                'message' => $validator->errors(),
            ];
            return response()->json($response, 400);
        }

        $content = request('content');
        DB::table('todolist')
            ->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'content' => $content,
            ]);
        return response()->json(['status' => true, 'message' => 'Data sukses Ditambahkan!']);
    }

    // mengupdate data (Update)
    public function postUpdate($id)
    {
        $content = request('content');
        DB::table('todolist')
            ->where('id', $id)
            ->update([
                'updated_at' => date('Y-m-d H:i:s'),
                'content' => $content,
            ]);
        return response()->json(['status' => true, 'message' => 'Data berhasil Diupdate!']);
    }

    // menghapus data (Delete)
    public function getDelete($id)
    {
        DB::table('todolist')
            ->where('id', $id)
            ->delete();
        return response()->json(['status' => true, 'message' => 'Data berhasil Dihapus!']);
    }
}
